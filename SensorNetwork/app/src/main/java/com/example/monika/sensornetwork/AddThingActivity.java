package com.example.monika.sensornetwork;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class AddThingActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn;
    JSONObject jsonObj = null;
    String thingId = null;
    EditText iotNameValue = null, iotDescriptionValue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_thing);

        iotNameValue = (EditText) findViewById(R.id.textThingIotNameNewChange);
        iotDescriptionValue = (EditText) findViewById(R.id.textThingIotDescriptionNewChange);

        btn = (Button) findViewById(R.id.buttonChangeThingNew);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        PostExample server = null;

        String iotNameValueS = iotNameValue.getText().toString();
        String iotDescriptionValueS = iotDescriptionValue.getText().toString();

        if (iotNameValueS.isEmpty() || iotDescriptionValueS.isEmpty()) {
            CharSequence text = "Name and description are mandatory!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
        } else {

            JSONObject thing = new JSONObject();
            try {
                thing.put("name", iotNameValueS);
                thing.put("description", iotDescriptionValueS);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                server = new PostExample(this, thing);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            server.execute();
        }

    }
}
