package com.example.monika.sensornetwork;

import java.io.IOException;

import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

class SensorUp {

    private static final OkHttpClient client = new OkHttpClient();;
    //private final OkHttpClient client = new OkHttpClient();

    public static String getData () throws Exception {
        Request request = new Request.Builder()
                .url("https://academic-hr-ad60.sensorup.com/display/v1.0/Things")
                .build();

        Response response = client.newCall(request).execute();
        //if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        /*Headers responseHeaders = response.headers();
        for (int i = 0; i < responseHeaders.size(); i++) {
            System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
        }

        System.out.println(response.body().string());*/
        return response.body().string();
    }

}
