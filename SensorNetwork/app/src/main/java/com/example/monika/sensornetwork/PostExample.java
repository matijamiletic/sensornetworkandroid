package com.example.monika.sensornetwork;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Matija on 6/6/2017.
 */

public class PostExample extends AsyncTask<String, Void, String> {

    private final Activity activity;
    private String entity = "";
    private String locations = "";
    private JSONObject jsonObjectToPost = null;
    private String returnValue = "";
    String json;

    PostExample(Activity activity, JSONObject obj) throws JSONException {
        this.activity = activity;
        this.jsonObjectToPost = obj;
        this.json = jsonObjectToPost.toString();
    }

    PostExample(Activity activity, JSONObject obj, String entity) throws JSONException {
        this.activity = activity;
        this.jsonObjectToPost = obj;
        this.json = jsonObjectToPost.toString();
        this.entity = entity;
    }

    private OkHttpClient client = new OkHttpClient();

    private static final MediaType JSON  = MediaType.parse("application/json;charset=UTF-8");

    String doPostRequest(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);

        String credential = Credentials.basic("academic", "8b59150a");

        Request request = new Request.Builder()
                .header("Content-Type", "application/json")
                .addHeader("Authorization", credential)
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();

        return response.body().string();
    }

    String doPatchRequest(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);

        String credential = Credentials.basic("academic", "8b59150a");

        Request request = new Request.Builder()
                .header("Content-Type", "application/json")
                .addHeader("Authorization", credential)
                .url(url)
                .patch(body)
                .build();
        Response response = client.newCall(request).execute();

        return response.body().string();
    }

    String doGetRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(String... params) {
        switch (this.entity) {
            case "location":
                doInBackgroundLocation();
                break;
            case "datastream":
                doInBackgroundDatastream();
                break;
            default:
                doInBackgroundThing();
                break;
        }
        return null;
    }

    @Override
    protected void onPreExecute() {}

    @Override
    protected void onProgressUpdate(Void... values) {}

    @Override
    protected void onPostExecute(String result) {

        switch (this.entity) {
            case "location":
                onPostExecuteLocation();
                break;
            case "datastream":
                onPostExecuteDatastream();
                break;
            default:
                onPostExecuteThing();
                break;
        }
    }

    private JSONArray unitsOfMeasurment = new JSONArray();

    private JSONObject collectedDataForPythonServerPost = null;

    private void doInBackgroundDatastream() {

        try {
            JSONObject degC = new JSONObject();
            degC.put("name", "Degree Celsius");
            degC.put("symbol", "degC");
            degC.put("definition", "http://www.qudt.org/qudt/owl/1.0.0/unit/Instances.html#DegreeCelsius");
            this.unitsOfMeasurment.put(degC);

            JSONObject percentage  = new JSONObject();
            percentage .put("name", "Percentage");
            percentage .put("symbol", "%");
            percentage .put("definition", "https://en.wikipedia.org/wiki/Percentage");
            this.unitsOfMeasurment.put(percentage );

            JSONObject bool  = new JSONObject();
            bool .put("name", "True/False");
            bool .put("symbol", "none");
            bool .put("definition", "https://en.wikipedia.org/wiki/Boolean");
            this.unitsOfMeasurment.put(bool);

            JSONObject rad  = new JSONObject();
            rad .put("name", "Radian");
            rad .put("symbol", "rad");
            rad .put("definition", "https://en.wikipedia.org/wiki/Humidity");
            this.unitsOfMeasurment.put(rad);

            JSONObject countBt  = new JSONObject();
            countBt .put("name", "Count");
            countBt .put("symbol", "none");
            countBt .put("definition", "https://en.wikipedia.org/wiki/Number");

            this.unitsOfMeasurment.put(countBt );

            JSONObject datastreamFromGet = new JSONObject(this.json);
            JSONObject thingData = datastreamFromGet.getJSONObject("Thing");
            String thingId = thingData.getString("@iot.id");
            JSONObject datastreamSensor = datastreamFromGet.getJSONObject("Sensor");
            String sensorId = datastreamSensor.getString("@iot.id");
            String sensorType="";
            if (Integer.parseInt(sensorId) == 4) sensorType = "DHT11";
            else if (Integer.parseInt(sensorId) == 1594) sensorType = "BISS0001";
            else if (Integer.parseInt(sensorId) == 5122) sensorType = "Bluetooth";

            JSONObject observedProperty1 = new JSONObject(), observedProperty2 = new JSONObject();
            JSONObject unit1 = new JSONObject(), unit2 = new JSONObject();

            collectedDataForPythonServerPost = new JSONObject();
            collectedDataForPythonServerPost.put("type", "new component");
            collectedDataForPythonServerPost.put("thingId", thingId);
            collectedDataForPythonServerPost.put("sensorType", sensorType);
            collectedDataForPythonServerPost.put("sensorId", sensorId);

            if (sensorId.equals("1594")) {

                String label = "movement";

                observedProperty1.put("@iot.id", "1602");
                unit1 = bool;

                datastreamFromGet.put("ObservedProperty", observedProperty1);
                datastreamFromGet.put("unitOfMeasurement", unit1);

                String jsonToPost = datastreamFromGet.toString();
                String datastreamFromPost = doPostRequest("https://academic-hr-ad60.sensorup.com/v1.0/Datastreams", jsonToPost);

                JSONObject dfp = new JSONObject(datastreamFromPost);
                String datastreamId = dfp.getString("@iot.id");

                String thingBefore = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things("+thingId+")");
                JSONObject thingBeforeObj = new JSONObject(thingBefore);

                JSONObject patchProperties = new JSONObject();

                JSONObject properties;
                if (thingBeforeObj.getJSONObject("properties") != null) {
                    properties = thingBeforeObj.getJSONObject("properties");
                } else {
                    properties = new JSONObject();
                }

                JSONObject sensor, sensorOn, condition;
                if (properties.has("BISS0001")) {
                    sensor = properties.getJSONObject("BISS0001");
                    sensorOn = sensor.getJSONObject("sensorOn");
                    condition = sensor.getJSONObject("condition");
                } else {
                    sensor = new JSONObject();
                    sensorOn = new JSONObject();
                    condition = new JSONObject();
                }

                sensorOn.put(datastreamId, "True");
                sensor.put("sensorOn", sensorOn);

                condition.put(datastreamId, "none");
                sensor.put("condition", condition);

                properties.put("BISS0001", sensor);

                patchProperties.put("properties", properties);


                String thingPost = doPatchRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things("+thingId+")", patchProperties.toString());

                this.returnValue = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things("+thingId+")?$expand=Locations");
                this.locations = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Locations");

                JSONObject tempJsonDatastream = new JSONObject();
                tempJsonDatastream.put(label, datastreamId);
                collectedDataForPythonServerPost.put("datastreams", tempJsonDatastream);


            } else if (sensorId.equals("5122")) {

                String label = "devices_count";

                observedProperty1.put("@iot.id", "5144");
                unit1 = countBt;

                datastreamFromGet.put("ObservedProperty", observedProperty1);
                datastreamFromGet.put("unitOfMeasurement", unit1);

                String jsonToPost = datastreamFromGet.toString();
                String datastreamFromPost = doPostRequest("https://academic-hr-ad60.sensorup.com/v1.0/Datastreams", jsonToPost);

                JSONObject dfp = new JSONObject(datastreamFromPost);
                String datastreamId = dfp.getString("@iot.id");

                String thingBefore = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things("+thingId+")");
                JSONObject thingBeforeObj = new JSONObject(thingBefore);

                JSONObject patchProperties = new JSONObject();

                JSONObject properties;
                if (thingBeforeObj.getJSONObject("properties") != null) {
                    properties = thingBeforeObj.getJSONObject("properties");
                } else {
                    properties = new JSONObject();
                }

                JSONObject sensor, sensorOn, condition;
                if (properties.has("Bluetooth")) {
                    sensor = properties.getJSONObject("Bluetooth");
                    sensorOn = sensor.getJSONObject("sensorOn");
                    condition = sensor.getJSONObject("condition");
                } else {
                    sensor = new JSONObject();
                    sensorOn = new JSONObject();
                    condition = new JSONObject();
                }

                sensorOn.put(datastreamId, "True");
                sensor.put("sensorOn", sensorOn);

                condition.put(datastreamId, "none");
                sensor.put("condition", condition);

                properties.put("Bluetooth", sensor);

                patchProperties.put("properties", properties);


                String thingPost = doPatchRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things("+thingId+")", patchProperties.toString());

                this.returnValue = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things("+thingId+")?$expand=Locations");
                this.locations = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Locations");

                JSONObject tempJsonDatastream = new JSONObject();
                tempJsonDatastream.put(label, datastreamId);
                collectedDataForPythonServerPost.put("datastreams", tempJsonDatastream);

            } else if (sensorId.equals("4")) {

                String labelTemperature = "temperature";
                String labelHumidity = "humidity";

                observedProperty1.put("@iot.id", "777");
                unit1 = percentage;

                JSONObject dataStreamToSend = datastreamFromGet;

                dataStreamToSend.put("ObservedProperty", observedProperty1);
                dataStreamToSend.put("unitOfMeasurement", unit1);

                String beforeName = dataStreamToSend.getString("name");
                String humName = beforeName + " - Humidity";
                dataStreamToSend.remove("name");
                dataStreamToSend.put("name", humName);
                String jsonToPost = dataStreamToSend.toString();

                //datastream za humidity
                String firstDatastream = doPostRequest("https://academic-hr-ad60.sensorup.com/v1.0/Datastreams", jsonToPost);

                dataStreamToSend = datastreamFromGet;

                observedProperty2.put("@iot.id", "5");
                unit2 = degC;

                dataStreamToSend.put("ObservedProperty", observedProperty2);
                dataStreamToSend.put("unitOfMeasurement", unit2);

                String tempName = beforeName + " - Temperature";
                dataStreamToSend.remove("name");
                dataStreamToSend.put("name", tempName);
                jsonToPost = dataStreamToSend.toString();
                //datastream za temperature
                String secondDatastream = doPostRequest("https://academic-hr-ad60.sensorup.com/v1.0/Datastreams", jsonToPost);

                //UDP SLANJE NA PYTHON SERVER
                JSONObject humidityDatastream = new JSONObject(firstDatastream);
                JSONObject temperatureDatastream = new JSONObject(secondDatastream);

                String humidityDatastreamId = humidityDatastream.getString("@iot.id");
                String temperatureDatastreamId = temperatureDatastream.getString("@iot.id");

                String thingBefore = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things("+thingId+")");
                JSONObject thingBeforeObj = new JSONObject(thingBefore);

                JSONObject patchProperties = new JSONObject();
                JSONObject properties;
                if (thingBeforeObj.getJSONObject("properties") != null) {
                    properties = thingBeforeObj.getJSONObject("properties");
                } else {
                    properties = new JSONObject();
                }

                JSONObject sensor, sensorOn, condition;
                if (properties.has("DHT11")) {
                    sensor = properties.getJSONObject("DHT11");
                    sensorOn = sensor.getJSONObject("sensorOn");
                    condition = sensor.getJSONObject("condition");
                } else {
                    sensor = new JSONObject();
                    sensorOn = new JSONObject();
                    condition = new JSONObject();
                }

                sensorOn.put(humidityDatastreamId + "-" + temperatureDatastreamId, "True");
                sensor.put("sensorOn", sensorOn);

                condition.put(humidityDatastreamId, "none");
                condition.put(temperatureDatastreamId, "none");
                sensor.put("condition", condition);

                properties.put("DHT11", sensor);

                patchProperties.put("properties", properties);

                String thingPost = doPatchRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things("+thingId+")", patchProperties.toString());

                this.returnValue = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things("+thingId+")?$expand=Locations");
                this.locations = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Locations");

                JSONObject tempJsonDatastream = new JSONObject();
                tempJsonDatastream.put(labelHumidity, humidityDatastreamId);
                tempJsonDatastream.put(labelTemperature, temperatureDatastreamId);
                collectedDataForPythonServerPost.put("datastreams", tempJsonDatastream);
            }

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private void doInBackgroundThing() {
        try {
            this.returnValue = doPostRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things", json);

            JSONObject thing = new JSONObject(this.returnValue);
            String id = thing.getString("@iot.id");

            this.returnValue = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Things("+id+")?$expand=Locations");
            this.locations = doGetRequest("https://academic-hr-ad60.sensorup.com/v1.0/Locations");
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void onPostExecuteThing() {
        /*int duration = Toast.LENGTH_LONG;
        Toast.makeText(activity, "New thing successfully added, opening full view.", duration).show();*/

        //UDP SLANJE NA PYTHON SERVER
        String thingId="";
        try {
            JSONObject thing = new JSONObject(this.returnValue);
            thingId = thing.getString("@iot.id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String msgToSend = "{'type':'new component', 'deviceId':" + thingId + "}";
        ClientUdpSender sendToPythonServer = new ClientUdpSender(msgToSend);
        JSONObject jsonObj=null;
        try {
            String response = sendToPythonServer.execute().get();
            jsonObj = new JSONObject(response);
        } catch (InterruptedException | ExecutionException | JSONException e) {
            e.printStackTrace();
        }

        if (jsonObj.has("success")) {

            String successResponse = null;
            try {
                successResponse = jsonObj.getString("success");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (successResponse.equals("True")) {

                Log.d("PYTHON_SERVER", "Success true");
                int dur = Toast.LENGTH_LONG;
                Toast.makeText(activity, "New device successfully added, opening full view.", dur).show();


            }else{

                Log.d("PYTHON_SERVER", "No success key true in response");
                //msgToSend
                DataHolder.setUdpMessage(msgToSend);
                int dur = Toast.LENGTH_LONG;
                Toast.makeText(activity, "Adding device failed on python server", dur).show();

            }

        }else{

            Log.d("PYTHON_SERVER", "No success key in response");
            DataHolder.setUdpMessage(msgToSend);
            int dur = Toast.LENGTH_LONG;
            Toast.makeText(activity, "Adding device failed on python server", dur).show();
        }

        final Intent intent = new Intent(activity, DisplayThingTabActivity.class);
        intent.putExtra("things", this.returnValue);
        intent.putExtra("locations", this.locations);

        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    Thread.sleep(3500); // As I am using LENGTH_LONG in Toast
                    activity.startActivity(intent);
                    activity.finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    private void onPostExecuteDatastream() {

        int dur = Toast.LENGTH_LONG;
        Toast.makeText(activity, "New sensor successfully added to sensorUp", dur).show();

        Log.d("PYTHON_SERVER", collectedDataForPythonServerPost.toString());
        ClientUdpSender sendToPyServer = new ClientUdpSender(collectedDataForPythonServerPost.toString());
        JSONObject jsonObj=null;
        try {
            String response = sendToPyServer.execute().get();
            jsonObj = new JSONObject(response);
        } catch (InterruptedException | ExecutionException | JSONException e) {
            e.printStackTrace();
        }

        int duration = Toast.LENGTH_LONG;
        if (jsonObj.has("success")) {

            String successResponse = null;
            try {
                successResponse = jsonObj.getString("success");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (successResponse.equals("True")) {

                Log.d("PYTHON_SERVER", "Success true");
                Toast.makeText(activity, "New sensor successfully added, opening full view.", duration).show();


            }else{

                Log.d("PYTHON_SERVER", "No success key true in response");
                DataHolder.setUdpMessage(collectedDataForPythonServerPost.toString());
                Toast.makeText(activity, "Adding device failed on python server", duration).show();

            }

        }else{

            Log.d("PYTHON_SERVER", "No success key in response");
            DataHolder.setUdpMessage(collectedDataForPythonServerPost.toString());
            Toast.makeText(activity, "Adding device failed on python server", duration).show();
        }

        final Intent intent = new Intent(activity, DisplayThingTabActivity.class);
        intent.putExtra("things", this.returnValue);
        intent.putExtra("locations", this.locations);
        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    Thread.sleep(3500); // As I am using LENGTH_LONG in Toast
                    activity.startActivity(intent);
                    activity.finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    private void onPostExecuteLocation() {

        int duration = Toast.LENGTH_LONG;
        Toast.makeText(activity, "New location successfully added, opening full view.", duration).show();

        final Intent intent = new Intent(activity, DisplayLocationTab.class);
        intent.putExtra("location", this.returnValue);
        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    Thread.sleep(3500); // As I am using LENGTH_LONG in Toast
                    activity.startActivity(intent);
                    activity.finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void doInBackgroundLocation(String... params) {

        JSONObject locationToSend = new JSONObject();
        JSONObject loc = new JSONObject();

        try {
            locationToSend.put("name", jsonObjectToPost.getString("name"));
            locationToSend.put("description", jsonObjectToPost.getString("description"));
            locationToSend.put("encodingType", "application/vnd.geo+json");
            loc.put("type", "Point");

            JSONArray coords = new JSONArray();
            coords.put(Double.parseDouble(jsonObjectToPost.getString("x")));
            coords.put(Double.parseDouble(jsonObjectToPost.getString("y")));

            loc.put("coordinates", coords);
            locationToSend.put("location", loc);

            this.returnValue = doPostRequest("https://academic-hr-ad60.sensorup.com/v1.0/Locations", locationToSend.toString());

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }
}

