package com.example.monika.sensornetwork;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    private Boolean isFabOpen = false;
    private FloatingActionButton fab,fab1thing,fab2location, fab3sensor;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    BackgroundServiceForUdp backgroundService;
    Intent backgroundServiceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("start", "start activity");

        backgroundService = new BackgroundServiceForUdp();
        backgroundServiceIntent = new Intent(this, backgroundService.getClass());
        if (!isMyServiceRunning(backgroundService.getClass())) {
            startService(backgroundServiceIntent);
        }

        //       Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        fab = (FloatingActionButton)findViewById(R.id.fab);
        fab1thing = (FloatingActionButton)findViewById(R.id.fab1thing);
        fab2location = (FloatingActionButton)findViewById(R.id.fab2location);
        fab3sensor = (FloatingActionButton)findViewById(R.id.fab3Sensor);
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);
        fab.setOnClickListener(this);
        fab1thing.setOnClickListener(this);
        fab2location.setOnClickListener(this);
        fab3sensor.setOnClickListener(this);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.d ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.d ("isMyServiceRunning?", false+"");
        return false;
    }
    /** Called when the user taps the Send button */
    public void loadThings (View view) {
        // Do something in response to button
        Intent intent = new Intent(this, DisplayThingsActivity.class);
        //EditText editText = "Nesto";
        String message = "neki tekst";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    public void runHistory (View view) {
        Intent intent = new Intent(this, NotificationHistory.class);
        startActivity(intent);
    }

    public void loadSensors (View view) {

        Intent intent = new Intent(this, DisplaySensorsActivity.class);
        //EditText editText = "Nesto";
        String message = "neki tekst";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

    }

    public void loadLocations (View view) {

        Intent intent = new Intent(this, DisplayLocationsActivity.class);
        startActivity(intent);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.fab:
                animateFAB();
                break;
            case R.id.fab1thing:
                Intent intent = new Intent(this, AddThingActivity.class);
                startActivity(intent);
                Log.d("Raj", "Fab 1");
                break;
            case R.id.fab2location:
                Intent intent2 = new Intent(this, AddLocationActivity.class);
                startActivity(intent2);
                Log.d("Raj", "Fab 2");
                break;
            case R.id.fab3Sensor:
                Intent intent3 = new Intent(this, AddSensorActivity.class);
                startActivity(intent3);
                Log.d("Raj", "Fab 3");
                break;
        }
    }

    public void animateFAB(){

        if(isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1thing.startAnimation(fab_close);
            fab2location.startAnimation(fab_close);
            fab3sensor.startAnimation(fab_close);
            fab1thing.setClickable(false);
            fab2location.setClickable(false);
            fab3sensor.setClickable(false);
            isFabOpen = false;

        } else {

            fab.startAnimation(rotate_forward);
            fab1thing.startAnimation(fab_open);
            fab2location.startAnimation(fab_open);
            fab3sensor.startAnimation(fab_open);
            fab1thing.setClickable(true);
            fab2location.setClickable(true);
            fab3sensor.setClickable(true);
            isFabOpen = true;

        }
    }

}
