package com.example.monika.sensornetwork;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DisplayDatasreamsForEdit extends AppCompatActivity {

    JSONObject jsonObj = null;
    JSONObject sensorObject, thingObject, thingPropertiesJson=null;
    JSONArray datastreamArray;
    String[] datastreamId;

    private ArrayList<String> listOfDatastreams=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_datasreams_for_edit);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null) {
            try {

                sensorObject = new JSONObject(bundle.getString("sensor"));
                datastreamArray = new JSONArray(bundle.getString("datastreams"));
                thingObject = new JSONObject(bundle.getString("thing"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        JSONObject jsonObject;
        ListView lv = (ListView) findViewById(R.id.datastreamList);
        int listLenth = datastreamArray.length();
        listOfDatastreams = new ArrayList<String>(Integer.parseInt(String.valueOf(listLenth)));
        String jsonString="", datastreamName="";
        datastreamId = new String[listLenth];

        for (int i=0; i<datastreamArray.length(); i++){

            try {
                jsonString = datastreamArray.getString(i);
                jsonObject = new JSONObject(jsonString);
                datastreamName = jsonObject.getString("name");
                datastreamId[i] = jsonObject.getString("@iot.id");
                listOfDatastreams.add(datastreamName);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listOfDatastreams);

        lv.setAdapter(adapter);

        try {
            thingPropertiesJson = thingObject.getJSONObject("properties");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(getApplicationContext(), EditSensorConfiration.class);
                intent.putExtra("properties", thingPropertiesJson.toString());
                intent.putExtra("sensor", sensorObject.toString());
                intent.putExtra("datastream", datastreamArray.toString());
                intent.putExtra("thing", thingObject.toString());
                intent.putExtra("datastreamId", datastreamId[position]);
                startActivity(intent);

            }
        });


    }

}
