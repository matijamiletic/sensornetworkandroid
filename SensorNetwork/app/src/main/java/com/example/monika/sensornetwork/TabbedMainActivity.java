package com.example.monika.sensornetwork;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.android.gms.nearby.messages.Message;

public class TabbedMainActivity extends AppCompatActivity implements View.OnClickListener{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    private Boolean isFabOpen = false;
    private FloatingActionButton fabMain,fab1thing,fab2location, fab3sensor, fabHistory;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    BackgroundServiceForUdp backgroundService;
    Intent backgroundServiceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        fabMain = (FloatingActionButton)findViewById(R.id.fab);
        fab1thing = (FloatingActionButton)findViewById(R.id.fab1thing);
        fab2location = (FloatingActionButton)findViewById(R.id.fab2location);
        fab3sensor = (FloatingActionButton)findViewById(R.id.fab3Sensor);
        fabHistory = (FloatingActionButton)findViewById(R.id.fabHistory);

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);
        fabMain.setOnClickListener(this);
        fab1thing.setOnClickListener(this);
        fab2location.setOnClickListener(this);
        fab3sensor.setOnClickListener(this);
        fabHistory.setOnClickListener(this);

        backgroundService = new BackgroundServiceForUdp();
        backgroundServiceIntent = new Intent(this, backgroundService.getClass());
        if (!isMyServiceRunning(backgroundService.getClass())) {
            startService(backgroundServiceIntent);
        }

    }

    public void runHistory (View view) {
        Intent intent = new Intent(this, NotificationHistory.class);
        startActivity(intent);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.d ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.d ("isMyServiceRunning?", false+"");
        return false;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.fab:
                animateFAB();
                break;
            case R.id.fab1thing:
                Intent intent = new Intent(this, AddThingActivity.class);
                startActivity(intent);
                Log.d("Raj", "Fab 1");
                break;
            case R.id.fab2location:
                Intent intent2 = new Intent(this, AddLocationActivity.class);
                startActivity(intent2);
                Log.d("Raj", "Fab 2");
                break;
            case R.id.fab3Sensor:
                Intent intent3 = new Intent(this, AddSensorActivity.class);
                startActivity(intent3);
                Log.d("Raj", "Fab 3");
                break;
            case R.id.fabHistory:
                Intent intentHistory = new Intent(this, NotificationHistory.class);
                startActivity(intentHistory);
                break;
        }
    }

    public void animateFAB(){

        if(isFabOpen){

            fabMain.startAnimation(rotate_backward);
            fab1thing.startAnimation(fab_close);
            fab2location.startAnimation(fab_close);
            fab3sensor.startAnimation(fab_close);
            fab1thing.setClickable(false);
            fab2location.setClickable(false);
            fab3sensor.setClickable(false);
            isFabOpen = false;

        } else {

            fabMain.startAnimation(rotate_forward);
            fab1thing.startAnimation(fab_open);
            fab2location.startAnimation(fab_open);
            fab3sensor.startAnimation(fab_open);
            fab1thing.setClickable(true);
            fab2location.setClickable(true);
            fab3sensor.setClickable(true);
            isFabOpen = true;

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tabbed_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            switch (position){
                case 0:
                    DeviceTabMain1 tab1 = new DeviceTabMain1();
                    return tab1;
                case 1:
                    SensorsTabMain2 tab2 = new SensorsTabMain2();
                    return tab2;
                case 2:
                    LocationsTabMain3 tab3 = new LocationsTabMain3();
                    return tab3;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "DEVICES";
                case 1:
                    return "SENSORS";
                case 2:
                    return "LOCATIONS";
            }
            return null;
        }
    }
}
