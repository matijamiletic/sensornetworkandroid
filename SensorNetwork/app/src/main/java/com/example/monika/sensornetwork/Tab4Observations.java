package com.example.monika.sensornetwork;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Monika on 25.5.2017..
 */

public class Tab4Observations extends Fragment{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab4observation, container, false);

        ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        Bundle bundle=getArguments();
        if(bundle!=null) {

            if (bundle.containsKey("things")) {
                try {
                    JSONObject jsonObj = new JSONObject(bundle.getString("things"));

                    ListView lv = (ListView) rootView.findViewById(R.id.thingsDatastreamView);

                    Activity activity = getActivity();

                    String link = jsonObj.getString("Datastreams@iot.navigationLink");
                    GetExample example = new GetExample(lv, activity, "datastreams", link, progressBar);
                    example.execute();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return rootView;
    }
}
