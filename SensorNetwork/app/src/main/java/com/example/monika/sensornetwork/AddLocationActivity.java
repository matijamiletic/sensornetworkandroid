package com.example.monika.sensornetwork;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class AddLocationActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, View.OnClickListener {

    private GoogleMap mMap;
    private Marker now;
    Button btn;
    public FusedLocationProviderClient mFusedLocationClient;
    //EditText iotNameValue = null, iotDescriptionValue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.gmap);
        mapFragment.getMapAsync(this);
        btn = (Button) findViewById(R.id.buttonAddLocation);
        btn.setOnClickListener(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (now != null) {
            now.remove();
        }

        String address = getCompleteAddressString(latLng.latitude, latLng.longitude);
        Log.d("address", address);

        now = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .snippet(address));

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                LinearLayout info = new LinearLayout(getApplicationContext());
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(getApplicationContext());
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(getApplicationContext());
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        });


        EditText editTextX = (EditText) findViewById(R.id.textLocationCoordinatesValueX);
        editTextX.setText(String.format(Locale.US, "%.4f", latLng.latitude));

        EditText editTextY = (EditText) findViewById(R.id.textLocationCoordinatesValueY);
        editTextY.setText(String.format(Locale.US, "%.4f", latLng.longitude));

    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.d("Current loction address", "" + strReturnedAddress.toString());
            } else {
                Log.d("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Current loction address", "Canont get Address!");
        }
        return strAdd;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        final TextView x = (TextView) findViewById(R.id.textLocationCoordinatesValueX);
        final TextView y = (TextView) findViewById(R.id.textLocationCoordinatesValueY);


        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.setOnMapClickListener(this);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            LatLng rijeka = new LatLng(45.3271, 14.4422);

            x.setText("45.3271");
            y.setText("14.4422");

            String address = getCompleteAddressString(45.3271, 14.4422);

            now = mMap.addMarker(new MarkerOptions().position(rijeka).snippet(address));

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    LinearLayout info = new LinearLayout(getApplicationContext());
                    info.setOrientation(LinearLayout.VERTICAL);

                    TextView title = new TextView(getApplicationContext());
                    title.setTextColor(Color.BLACK);
                    title.setGravity(Gravity.CENTER);
                    title.setTypeface(null, Typeface.BOLD);
                    title.setText(marker.getTitle());

                    TextView snippet = new TextView(getApplicationContext());
                    snippet.setTextColor(Color.GRAY);
                    snippet.setText(marker.getSnippet());

                    info.addView(title);
                    info.addView(snippet);

                    return info;
                }
            });

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(rijeka,17));

            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // ...
                            LatLng latLong = new LatLng(location.getLatitude(), location.getLongitude());

                            x.setText(String.format (Locale.US, "%.4f", latLong.latitude));
                            y.setText(String.format (Locale.US, "%.4f", latLong.longitude));

                            String address = getCompleteAddressString(latLong.latitude, latLong.longitude);

                            now = mMap.addMarker(new MarkerOptions().position(latLong).snippet(address));

                            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                @Override
                                public View getInfoWindow(Marker arg0) {
                                    return null;
                                }

                                @Override
                                public View getInfoContents(Marker marker) {

                                    LinearLayout info = new LinearLayout(getApplicationContext());
                                    info.setOrientation(LinearLayout.VERTICAL);

                                    TextView title = new TextView(getApplicationContext());
                                    title.setTextColor(Color.BLACK);
                                    title.setGravity(Gravity.CENTER);
                                    title.setTypeface(null, Typeface.BOLD);
                                    title.setText(marker.getTitle());

                                    TextView snippet = new TextView(getApplicationContext());
                                    snippet.setTextColor(Color.GRAY);
                                    snippet.setText(marker.getSnippet());

                                    info.addView(title);
                                    info.addView(snippet);

                                    return info;
                                }
                            });

                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLong,17));

                            Log.d("lokacija", location.toString());
                        }
                    }
                });

        //Log.d("maps", "maps");
    }

    public void onMapSearch(View view) {
        EditText locationSearch = (EditText) findViewById(R.id.editText);
        String location = locationSearch.getText().toString();
        List<Address> addressList = null;

        if (location != null || !location.equals("")) {
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location, 1);

            } catch (IOException e) {
                e.printStackTrace();
            }
            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            //mMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));

            if(now != null){
                now.remove();
            }

            now = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title("TocuhePoint"));

            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

            EditText editTextX = (EditText)findViewById(R.id.textLocationCoordinatesValueX);
            editTextX.setText(String.format (Locale.US, "%.4f", latLng.latitude));

            EditText editTextY = (EditText)findViewById(R.id.textLocationCoordinatesValueY);
            editTextY.setText(String.format (Locale.US, "%.4f", latLng.longitude));

        }
    }

    @Override
    public void onClick(View v) {

        PostExample server = null;

        EditText iotNameValue = (EditText) findViewById(R.id.textLocationNameValue);
        EditText iotDescriptionValue = (EditText) findViewById(R.id.textLocationDescriptionValue);
        EditText iotXValue = (EditText) findViewById(R.id.textLocationCoordinatesValueX);
        EditText iotYValue = (EditText) findViewById(R.id.textLocationCoordinatesValueY);

        String iotNameValueS = iotNameValue.getText().toString();
        String iotDescriptionValueS = iotDescriptionValue.getText().toString();
        String iotXValueS = iotXValue.getText().toString();
        String iotYValueS = iotYValue.getText().toString();

        if (iotNameValueS.isEmpty() || iotDescriptionValueS.isEmpty() || iotXValueS.isEmpty() || iotYValueS.isEmpty()) {
            CharSequence text = "All fileds are mandatory!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
        } else {

            JSONObject location = new JSONObject();
            try {
                location.put("name", iotNameValueS);
                location.put("description", iotDescriptionValueS);
                location.put("x", iotXValueS);
                location.put("y", iotYValueS);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                server = new PostExample(this, location, "location");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            server.execute();
        }

        //Log.d("name value", iotNameValueS);
    }
}
