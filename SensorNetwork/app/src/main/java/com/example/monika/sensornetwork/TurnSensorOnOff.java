package com.example.monika.sensornetwork;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class TurnSensorOnOff extends AppCompatActivity implements View.OnClickListener{

    Button buttonState;
    TextView senState;
    String sensorState="", datastreamId="", sensorType="", thingId="", wholeKey="";
    JSONObject properties, sensorObject;
    JSONArray datastreamArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_turn_sensor_on_off);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null) {

            sensorState = bundle.getString("sensorState");
            datastreamId = bundle.getString("datastreamId");
            sensorType = bundle.getString("sensorType");
            thingId = bundle.getString("thingId");
            wholeKey = bundle.getString("wholeKey");
            if (wholeKey.equals("none")) wholeKey = datastreamId;

            try {
                properties = new JSONObject(bundle.getString("properties"));
                sensorObject = new JSONObject(bundle.getString("sensor"));
                datastreamArray = new JSONArray(bundle.getString("datastreamArray"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        buttonState = (Button) findViewById(R.id.buttonTurnOnOff);
        buttonState.setOnClickListener(this);

        senState = (TextView) findViewById(R.id.snState);

        if (sensorState.equals("True")) {
            buttonState.setText("Turn off");
            senState.setText("Sensor is on");
        } else {
            buttonState.setText("Turn on");
            senState.setText("Sensor is off");
        }
    }

    @Override
    public void onClick(View v) {

        String successResponse="";
        JSONObject thing, sendProperties;
        PatchExample server;

        if (v.getId() == buttonState.getId()){

            Log.d("UDP-btn", "unutra");

            String UDPmsg="", newSensorState="";
            if (sensorState.equals("True")) {
                UDPmsg = "{'type':'sensorOnOff','datastreamId':" + datastreamId + ", 'sensorOn':'False'}";
                newSensorState = "False";
            }else {
                UDPmsg = "{'type':'sensorOnOff','datastreamId':" + datastreamId + ", 'sensorOn':'True'}";
                newSensorState = "True";
            }

            Log.d("UDP_S", "Pripremam podatke za slanje na UDP server...");
            ClientUdpSender cilentSender = new ClientUdpSender(UDPmsg);

            try {
                String response = cilentSender.execute().get();
                JSONObject jsonObj = new JSONObject(response);

                sendProperties = prepareData(properties, sensorType, "sensorOn", newSensorState);
                Log.d("UDP-dataend", sendProperties.toString());

                if (jsonObj.has("success")) {

                    successResponse = jsonObj.getString("success");
                    if (successResponse.equals("True")) {
                        Log.d("UDP", "Data updated on server UDP");
                        Log.d("UDP", "Sending on sensorUp...");
                        thing = new JSONObject();
                        sendProperties = prepareData(properties, sensorType, "sensorOn", newSensorState);
                        Log.d("UDP-dataend", sendProperties.toString());
                        thing.put("properties", sendProperties);
                        server = new PatchExample(this, thingId, thing, "displaySensor", sensorObject, datastreamArray);
                        server.execute();

                    } else {

                        Log.d("UDP_S", "Success key is set to false");
                        CharSequence text = "Data update failed on server UDP";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(this, text, duration);
                        toast.show();
                    }

                }else{
                    Log.d("UDP", "No success key in response");
                    CharSequence text = "Data update failed on server UDP";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(this, text, duration);
                    toast.show();
                }

            } catch (InterruptedException | JSONException | ExecutionException e) {
                e.printStackTrace();
            }

        }
    }

    private JSONObject prepareData(JSONObject thingProperties, String type, String sensorKey, String state) throws JSONException {

        JSONObject conditionObj = thingProperties.getJSONObject(type);

        JSONObject datastreamObj = conditionObj.getJSONObject(sensorKey);
        datastreamObj.remove(String.valueOf(wholeKey));
        datastreamObj.put(String.valueOf(wholeKey), state);

        return thingProperties;

    }
}
