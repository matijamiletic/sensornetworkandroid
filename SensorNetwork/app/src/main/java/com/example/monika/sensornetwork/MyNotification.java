package com.example.monika.sensornetwork;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Matija on 6/6/2017.
 */

public class MyNotification extends AsyncTask<String, Void, String> {

    JSONArray sensorData;
    NotificationManager notificationManager;
    Notification noti;
    private Context context;

    MyNotification(JSONArray data, Context context){
        this.context = context;
        this.sensorData = data;
    }

    @Override
    protected String doInBackground(String... params) {

        //notification start

        Intent resultIntent = new Intent(this.context, NotificationResultActivity.class);
        resultIntent.putExtra("sensors", this.sensorData.toString());
        PendingIntent pIntent = PendingIntent.getActivity(this.context, (int) System.currentTimeMillis(), resultIntent, 0);

        noti = new Notification.Builder(this.context)
                .setContentTitle("SensorNetwork notification")
                .setContentText("Click to see data").setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntent)
                .addAction(R.drawable.icon, "And more", pIntent).build();

        notificationManager = (NotificationManager) this.context.getSystemService(this.context.NOTIFICATION_SERVICE);
        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        //notification end

        return null;
    }

    @Override
    protected void onPreExecute() {}

    @Override
    protected void onProgressUpdate(Void... values) {}

    @Override
    protected void onPostExecute(String result) {
        notificationManager.notify((int) System.currentTimeMillis(), noti);
    }
}
