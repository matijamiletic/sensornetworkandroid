package com.example.monika.sensornetwork;

import org.json.JSONArray;

/**
 * Created by Matija on 7/4/2017.
 */

public class DataHolder {

    private static String[] data;
    private static JSONArray[] notifications;
    private static int lastIndex = -1;
    private static String udpMessage = "{'type':'notification check'}";

    public static String getUdpMessage() {return udpMessage;}
    public static void setUdpMessage(String receivedData) {DataHolder.udpMessage = receivedData;}

    public static String[] getData() {return data;}
    public static void setData(String[] receivedData) {DataHolder.data = receivedData;}

    public static int getLastIndex() {return lastIndex;}
    public static void setLastIndex(int index) {DataHolder.lastIndex = index;}

    public static JSONArray[] getNotificationObjects() {return notifications;}
    public static void setNotificationObjects(JSONArray[] notif) {DataHolder.notifications = notif;}

}
