package com.example.monika.sensornetwork;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class ChangeSensorConditionValue extends AppCompatActivity {

    String thingProperties, thingId, sensorType, sensor, datastream, thing, datastreamCondition="";
    EditText value;
    int datastreamId;
    JSONObject sensorObject=null, thingObject=null;
    JSONArray datastreamsArray = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_sensor_condition_value);

        thingProperties = getIntent().getExtras().getString("properties");
        thingId = getIntent().getExtras().getString("id");
        sensorType = getIntent().getExtras().getString("type");
        datastreamId = Integer.parseInt(getIntent().getExtras().getString("datastreamId"));

        thing = getIntent().getExtras().getString("thing");

        datastream = getIntent().getExtras().getString("datastream");
        sensor = getIntent().getExtras().getString("sensor");
        datastreamCondition = getIntent().getExtras().getString("datastreamCondition");
        try {
            sensorObject = new JSONObject(sensor);
            datastreamsArray = new JSONArray(datastream);
            thingObject = new JSONObject(thing);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        value = (EditText) findViewById(R.id.value);
        value.setHint(datastreamCondition);

    }

    public void save (View view) throws JSONException {
        String val, msg, successResponse;
        int numVal=2;
        JSONObject thing = null;
        PatchExample server = null;

        val = value.getText().toString();

        try{
            numVal = Integer.parseInt(val);
        }catch (NumberFormatException e) {
            CharSequence text = "Condition is not valid!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
            e.printStackTrace();
        }

        if (numVal==0 || numVal==1){
            Log.d("UDP", String.valueOf(numVal));
            msg = "{'type':'change conditions', 'datastreamId':" + datastreamId + ", 'condition':" + numVal + "}";

            Log.d("UDP_S", "Pripremam podatke za slanje na UDP server...");
            ClientUdpSender cilentSender = new ClientUdpSender(msg);
            try {
                String response = cilentSender.execute().get();
                JSONObject jsonObj = new JSONObject(response);
                Log.d("UDP", jsonObj.toString());
                if (jsonObj.has("success")) {

                    successResponse = jsonObj.getString("success");
                    //ako je success true salji na sensorUp izmjene
                    if (successResponse.equals("True")) {

                        Log.d("UDP_S", "Podaci spremljeni na UDP server");
                        Log.d("UDP_S", "Pripremam podatke za SensorUp server...");
                        //update podataka

                        JSONObject sendProperties = prepareData(thingProperties, sensorType, "condition", numVal);

                        try {
                            thing = new JSONObject();
                            thing.put("properties", sendProperties);
                            //TODO: redirect sredit
                            server = new PatchExample(this, thingId, thing, "displaySensor", sensorObject, datastreamsArray);
                            server.execute();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }else{

                        Log.d("UDP_S", "Success key is set to false");
                        CharSequence text = "Data update failed on server UDP";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(this, text, duration);
                        toast.show();
                    }
                }else{

                    Log.d("UDP", "No success key in response");
                    CharSequence text = "Data update failed on server UDP";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(this, text, duration);
                    toast.show();

                }

            } catch (InterruptedException | ExecutionException | JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            value.setText("0/1");
            CharSequence text = "Condition is not good!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(this, text, duration);
            toast.show();
        }
    }

    private JSONObject prepareData(String thingProperties, String type, String sensorKey,  int conditionValue) throws JSONException {

        JSONObject jsonObj = new JSONObject(thingProperties);
        JSONObject conditionObj = jsonObj.getJSONObject(type);
        JSONObject datastreamObj = conditionObj.getJSONObject(sensorKey);
        datastreamObj.remove(String.valueOf(datastreamId));
        datastreamObj.put(String.valueOf(datastreamId), conditionValue);

        return jsonObj;

    }



}
