package com.example.monika.sensornetwork;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class DisplayDatastreamsForOnOffNormal extends AppCompatActivity {

    JSONObject thing, properties, configuration, sensorState, sensor;
    JSONArray datastreams;
    String sensorType="", thingId="";
    String values[], keys[], datastreamIds[];
    private ArrayList<String> listOfDatastreams=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_datastreams_for_on_off_normal);

        ListView lv = (ListView) findViewById(R.id.datastreamListOnOffNormal);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null) {
            try {

                thing = new JSONObject(bundle.getString("thing"));
                thingId = thing.getString("@iot.id");
                sensor = new JSONObject(bundle.getString("sensor"));
                datastreams = new JSONArray(bundle.getString("datastreams"));
                sensorType = bundle.getString("sensorType");

                properties = thing.getJSONObject("properties");
                configuration = properties.getJSONObject(sensorType);
                sensorState = configuration.getJSONObject("sensorOn");


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        setLayout();


        final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listOfDatastreams);

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getApplicationContext(), TurnSensorOnOff.class);
                try {
                    intent.putExtra("sensorState", sensorState.getString(datastreamIds[position]));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                intent.putExtra("datastreamId", datastreamIds[position]);
                intent.putExtra("sensorType", sensorType);
                intent.putExtra("properties", properties.toString());
                intent.putExtra("sensor", sensor.toString());
                intent.putExtra("thingId", thingId);
                intent.putExtra("datastreamArray", datastreams.toString());
                intent.putExtra("wholeKey", "none");
                startActivity(intent);


            }
        });


    }

    private void setLayout(){

        int lenth = datastreams.length();
        listOfDatastreams = new ArrayList<String>(lenth);
        datastreamIds = new String[lenth];
        int l = 0;
        for(int j = 0; j < datastreams.length(); j++)
        {
            String jsonString = null;
            try {
                jsonString = datastreams.getString(j);
                JSONObject tempObject = new JSONObject(jsonString);
                listOfDatastreams.add(tempObject.getString("name"));
                datastreamIds[l] = tempObject.getString("@iot.id");
                l++;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /*Iterator<String> keysIterator = sensorState.keys();
        keys = new String[lenth];
        values = new String[lenth];
        int i=0;

        while (keysIterator.hasNext())
        {

            keys[i] = (String)keysIterator.next();
            try {
                values[i] = sensorState.getString(keys[i]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            i++;

        }*/

        /*
        listOfDatastreams = new ArrayList<String>(lenth);

        Log.d("UDP-comp", datastreams.toString());
        int l=0;
        //ako je vise datastremova od conditiona za on/off
        for(int j = 0; j < datastreams.length(); j++)
        {
            for (int k = 0; k < keys.length; k++){

                try {

                    String jsonString = datastreams.getString(j);
                    JSONObject tempObject = new JSONObject(jsonString);
                    if (tempObject.getString("@iot.id").equals(keys[k])){

                        Log.d("UDP-key", keys[k]);
                        Log.d("UDP-comp", tempObject.getString("name"));
                        listOfDatastreams.add(tempObject.getString("name"));
                        datastreamIds[l] = tempObject.getString("@iot.id");
                        l++;


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }*/


    }
}
