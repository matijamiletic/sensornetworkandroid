package com.example.monika.sensornetwork;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Objects;

/**
 * Created by Monika on 25.5.2017..
 */

public class LocationsTabMain3 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.locationstabmain3, container, false);

        ListView lv = (ListView) rootView.findViewById(R.id.locationsView);
        ProgressBar pb = (ProgressBar) rootView.findViewById(R.id.progressBarLocations);

        GetExample example = new GetExample(lv, getActivity(), "locations", pb);
        example.execute();

        return rootView;
    }
}
