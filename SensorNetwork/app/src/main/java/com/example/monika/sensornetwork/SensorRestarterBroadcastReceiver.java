package com.example.monika.sensornetwork;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Matija on 6/6/2017.
 */

public class SensorRestarterBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("UDP", "Service Stops! Restart...");
        context.startService(new Intent(context, BackgroundServiceForUdp.class));;
    }
}
