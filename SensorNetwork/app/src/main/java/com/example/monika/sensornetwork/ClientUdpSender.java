package com.example.monika.sensornetwork;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 * Created by Matija on 6/11/2017.
 */

public class ClientUdpSender extends AsyncTask<String, String, String> {

    int UDP_SERVER = (UdpConfiguration.getAppConfiguration()).getUDPServer();
    String ipAdress = (UdpConfiguration.getAppConfiguration()).getIPAdress();

    DatagramSocket udpSocket=null;
    DatagramPacket packet;
    String response="{'success':'False'}";
    String message="";

    ClientUdpSender(String message) {

        this.message = message;

    }

    @Override
    protected String doInBackground(String... params) {

        try {
            udpSocket= new DatagramSocket();
            //send message
            JSONObject tempObj = null;
            try {
                tempObj = new JSONObject(this.message);
            } catch (JSONException e) {
                e.printStackTrace();

            }

            String tempStr = tempObj.toString();
            byte[] messageBuff = new byte[8000];
            InetAddress serverAddr = InetAddress.getByName(ipAdress);
            messageBuff = (tempStr).getBytes();
            packet = new DatagramPacket(messageBuff, messageBuff.length,serverAddr, UDP_SERVER);
            udpSocket.send(packet);
            udpSocket.setSoTimeout(5000);

            //receive response
            messageBuff = new byte[8000];
            packet = new DatagramPacket(messageBuff,messageBuff.length);
            udpSocket.receive(packet);
            response = new String(messageBuff, 0, packet.getLength());

        } catch (SocketException e) {
            Log.e("UDP", "Socket Error:", e);
        }catch(SocketTimeoutException e){

            if(udpSocket.isConnected()){
                udpSocket.close();
            }

        }catch (Exception e) {
            Log.d("UDP", "Error:", e);
        }

        return response;
    }

    @Override
    protected void onPostExecute(String result) {

        Log.d("UDP", "UDP server received data -> Response: " + response);

    }
}
