package com.example.monika.sensornetwork;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Monika on 26.5.2017..
 */

public class Location2Edit extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener, View.OnClickListener {

    private GoogleMap mMap;
    private LatLng start;
    private String title;
    private Marker now;
    static EditText editTextX;
    EditText editTextY;
    EditText locationSearch;
    Button btn, btnPatch;
    double xVal, yVal;
    EditText iotNameValue, iotDescriptionValue;

    public Location2Edit() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.location2edit, container, false);
        editTextX = (EditText)rootView.findViewById(R.id.textLocationCoordinatesValueX_edit);
        editTextY = (EditText) rootView.findViewById(R.id.textLocationCoordinatesValueY_edit);
        locationSearch = (EditText) rootView.findViewById(R.id.editText_edit);

        editTextX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        editTextY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        btn = (Button) rootView.findViewById(R.id.search_button_edit) ;
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onMapSearch2(v);
            }

        });

        btnPatch = (Button) rootView.findViewById(R.id.buttonEditLocation);

        Bundle bundle=getArguments();
        if(bundle!=null) {

            if (bundle.containsKey("location")) {
                try {
                    final JSONObject locationObject = new JSONObject(bundle.getString("location"));

                    iotNameValue = (EditText) rootView.findViewById(R.id.textLocationNameValue_edit);
                    iotDescriptionValue = (EditText) rootView.findViewById(R.id.textLocationDescriptionValue_edit);

                    iotNameValue.setText(locationObject.getString("name"));
                    iotDescriptionValue.setText(locationObject.getString("description"));

                    JSONObject loc = locationObject.getJSONObject("location");
                    JSONArray coor = loc.getJSONArray("coordinates");

                    xVal = coor.getDouble(0);
                    yVal = coor.getDouble(1);

                    editTextX.setText(String.format (Locale.US, "%.4f", xVal));
                    editTextY.setText(String.format (Locale.US, "%.4f", yVal));
                    title = locationObject.getString("name");
                    start = new LatLng(xVal, yVal);

                    btnPatch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String name = iotNameValue.getText().toString();
                            String description = iotDescriptionValue.getText().toString();
                            String xC = editTextX.getText().toString();
                            String yC = editTextY.getText().toString();

                            JSONArray coords = new JSONArray();
                            try {
                                coords.put(Double.parseDouble(xC));
                                coords.put(Double.parseDouble(yC));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (name.isEmpty() || description.isEmpty() || xC.isEmpty() || yC.isEmpty()) {
                                int duration = Toast.LENGTH_LONG;
                                Toast.makeText(getActivity(), "Please enter all fields.", duration).show();
                            } else {
                                try {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("name", name);
                                    jsonObject.put("description", description);
                                    JSONObject locObj = new JSONObject();
                                    locObj.put("type", "Point");
                                    locObj.put("coordinates", coords);
                                    jsonObject.put("location", locObj);
                                    PatchExample patch = new PatchExample(getActivity(), locationObject.getString("@iot.id"), jsonObject, "patchLocation");
                                    patch.execute();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.gmaps_edit);
        mapFragment.getMapAsync(this);

        return rootView;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if(now != null){
            now.remove();

        }
        now = mMap.addMarker(new MarkerOptions()
                .position(start)
                .title(title));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(start,17));

        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.setOnMapClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onMapClick(LatLng latLng) {
        if(now != null){
            now.remove();

        }
        now = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(title));

        editTextX.setText(String.format (Locale.US, "%.4f", latLng.latitude));
        editTextY.setText(String.format (Locale.US, "%.4f", latLng.longitude));

    }

    public void onMapSearch2 (View view) {
        String location = locationSearch.getText().toString();
        List<Address> addressList = null;

        if (!location.equals("")) {
            Geocoder geocoder = new Geocoder(getActivity());
            try {
                addressList = geocoder.getFromLocationName(location, 1);

            } catch (IOException e) {
                e.printStackTrace();
            }
            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());

            if(now != null){
                now.remove();
            }

            now = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(title));

            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

            editTextX.setText(String.format (Locale.US, "%.4f", latLng.latitude));
            editTextY.setText(String.format (Locale.US, "%.4f", latLng.longitude));

        }
    }
}
