package com.example.monika.sensornetwork;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Monika on 26.5.2017..
 */

public class SensorTab2Edit extends Fragment implements View.OnClickListener {

    JSONObject jsonObj = null;
    JSONObject sensorObject, thingObject;
    JSONArray datastreamArray;
    Button buttonConfiguration, onOffButton;
    int sensorId=-1;
    String sensorType="";

    private ArrayList<String> listOfDatastreams=null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sensor2tabedit, container, false);

        Bundle bundle=getArguments();
        if(bundle!=null) {
            try {

                sensorObject = new JSONObject(bundle.getString("sensor"));
                datastreamArray = new JSONArray(bundle.getString("datastreams"));
                thingObject = new JSONObject(bundle.getString("thing"));
                sensorId = sensorObject.getInt("@iot.id");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (sensorId == 4) sensorType = "DHT11";
        else if (sensorId == 1594) sensorType = "BISS0001";
        else if (sensorId == 5122) sensorType = "Bluetooth";

        onOffButton = (Button) rootView.findViewById(R.id.buttonOnOff);
        onOffButton.setOnClickListener(this);
        buttonConfiguration = (Button) rootView.findViewById(R.id.buttonConfiguration);
        buttonConfiguration.setOnClickListener(this);
        /*JSONObject jsonObject;
        ListView lv = (ListView) rootView.findViewById(R.id.sensorTab2Edit);
        int listLenth = datastreamArray.length();
        listOfDatastreams = new ArrayList<String>(Integer.parseInt(String.valueOf(listLenth)));
        String jsonString="", datastreamName="";
        datastreamId = new String[listLenth];

        for (int i=0; i<datastreamArray.length(); i++){

            try {
                jsonString = datastreamArray.getString(i);
                jsonObject = new JSONObject(jsonString);
                datastreamName = jsonObject.getString("name");
                datastreamId[i] = jsonObject.getString("@iot.id");
                listOfDatastreams.add(datastreamName);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listOfDatastreams);

        lv.setAdapter(adapter);

        try {
            thingPropertiesJson = thingObject.getJSONObject("properties");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getActivity(), EditSensorConfiration.class);
                intent.putExtra("properties", thingPropertiesJson.toString());
                intent.putExtra("sensor", sensorObject.toString());
                intent.putExtra("datastream", datastreamArray.toString());
                intent.putExtra("thing", thingObject.toString());
                intent.putExtra("datastreamId", datastreamId[position]);
                startActivity(intent);


            }
        });*/

        return rootView;
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == buttonConfiguration.getId()) {

            Log.d("UDP", "in conf");
            Intent intent = new Intent(getActivity(), DisplayDatasreamsForEdit.class);
            intent.putExtra("sensor", sensorObject.toString());
            intent.putExtra("datastreams", datastreamArray.toString());
            intent.putExtra("thing", thingObject.toString());
            startActivity(intent);

        }
        else if(v.getId() == onOffButton.getId()){
            Log.d("UDP", "in on off");

            if (sensorType.equals("DHT11")){

                Intent intent = new Intent(getActivity(), DisplayDatastreamsForOnOff.class);
                intent.putExtra("thing", thingObject.toString());
                intent.putExtra("sensorType", sensorType);
                intent.putExtra("datastreams", datastreamArray.toString());
                intent.putExtra("sensor", sensorObject.toString());
                startActivity(intent);

            }
            else{
                Log.d("UDP", "NOVI ACTIVITY");
                Intent intent = new Intent(getActivity(), DisplayDatastreamsForOnOffNormal.class);
                intent.putExtra("thing", thingObject.toString());
                intent.putExtra("sensorType", sensorType);
                intent.putExtra("datastreams", datastreamArray.toString());
                intent.putExtra("sensor", sensorObject.toString());
                startActivity(intent);
            }

        }

    }
}

