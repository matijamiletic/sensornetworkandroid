package com.example.monika.sensornetwork;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DisplayObservationInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_observation_info);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();

        try {
            JSONObject jsonObj = new JSONObject(extras.getString("observation"));

            TextView iotIdValue = (TextView) findViewById(R.id.observationIotIdValue);
            TextView iotSelfLinkValue = (TextView) findViewById(R.id.observationIotSelfLinkValue);
            TextView iotPhenomenTimeValue = (TextView) findViewById(R.id.observationIotPhenomenTimeValue);
            TextView iotResultValue = (TextView) findViewById(R.id.observationIotResultValue);
            TextView iotResultTimeValue = (TextView) findViewById(R.id.observationIotResultTimeValue);
            TextView iotDatastreamsNavLink = (TextView) findViewById(R.id.observationDatastreamNavigationLinkValue);
            TextView iotFoINavLink = (TextView) findViewById(R.id.observationFeatureOfInterestNavigationLinkValue);

            iotIdValue.setText(String.valueOf(jsonObj.getInt("@iot.id")));


            String selfLink = jsonObj.getString("@iot.selfLink");
            selfLink = selfLink.substring(0, 38) + "display/" + selfLink.substring(38, selfLink.length());
            iotSelfLinkValue.setText(selfLink);

            String phenomenonTime = jsonObj.getString("phenomenonTime");
            phenomenonTime = phenomenonTime.replace("T", " ");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convertedDate=null;
            try {
                convertedDate = dateFormat.parse(phenomenonTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd.MM.yyyy. HH:mm:ss z");
            phenomenonTime = fmtOut.format(convertedDate);

            iotPhenomenTimeValue.setText(phenomenonTime);

            iotResultValue.setText(jsonObj.getString("result"));

            String resultTime = jsonObj.getString("resultTime");
            resultTime = resultTime.replace("T", " ");
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date convertedDate2=null;
            try {
                convertedDate2 = dateFormat2.parse(resultTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat fmtOut2 = new SimpleDateFormat("dd.MM.yyyy. HH:mm:ss z");
            resultTime = fmtOut2.format(convertedDate2);

            iotResultTimeValue.setText(resultTime);

            String selfLinkD = jsonObj.getString("Datastream@iot.navigationLink");
            selfLinkD = selfLinkD.substring(0, 38) + "display/" + selfLinkD.substring(38, selfLinkD.length());
            iotDatastreamsNavLink.setText(selfLinkD);

            String selfLinkF = jsonObj.getString("FeatureOfInterest@iot.navigationLink");
            selfLinkF = selfLinkF.substring(0, 38) + "display/" + selfLinkF.substring(38, selfLinkF.length());
            iotFoINavLink.setText(selfLinkF);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
